#!/usr/bin/env python
# -*- coding: utf-8 -*-
from sqlalchemy import Column, text
from sqlalchemy.dialects.mysql import BIGINT, TINYINT

from trest.db import Model as Base


class IdiomFollow(Base):
    __tablename__ = 'idiom_follow'

    id = Column(BIGINT(20), primary_key=True)
    type = Column(TINYINT(1), nullable=False, server_default=text("'0'"), comment='接龙类型: 0 未知  1 人机对局  2 多人对局')
    user_id = Column(BIGINT(20), comment='发起者用户ID')
    begin_at = Column(BIGINT(13), nullable=False, server_default=text("'0'"), comment='Unix时间戳毫秒单位')
    end_at = Column(BIGINT(13), nullable=False, server_default=text("'0'"), comment='Unix时间戳毫秒单位')
    status = Column(TINYINT(1), nullable=False, server_default=text("'1'"), comment='状态: -1 删除  0 禁用  1 启用')
    created_at = Column(BIGINT(13), nullable=False, server_default=text("'0'"), comment='创建记录Unix时间戳毫秒单位')
