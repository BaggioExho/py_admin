#!/usr/bin/env python
# -*- coding: utf-8 -*-
from sqlalchemy import Column, JSON, String, text
from sqlalchemy.dialects.mysql import BIGINT, TINYINT

from trest.db import Model as Base


class UserBinding(Base):
    __tablename__ = 'user_binding'

    id = Column(BIGINT(20), primary_key=True, comment='主键')
    user_id = Column(BIGINT(20), nullable=False, server_default=text("'0'"), comment='用户ID')
    type = Column(TINYINT(1), nullable=False, comment='绑定类型: 1 微信小程序  2 ')
    oppid = Column(String(80), nullable=False, server_default=text("''"), comment='第三方appid')
    openid = Column(String(80), nullable=False, server_default=text("''"), comment='第三方平台openid')
    info = Column(JSON, nullable=False, comment='从第三方获取的账户信息')
    updated_at = Column(BIGINT(13), nullable=False, server_default=text("'0'"), comment='更新记录Unix时间戳毫秒单位')
    created_at = Column(BIGINT(13), nullable=False, server_default=text("'0'"), comment='创建记录Unix时间戳毫秒单位')
