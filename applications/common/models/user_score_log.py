#!/usr/bin/env python
# -*- coding: utf-8 -*-
from sqlalchemy import Column, String, text
from sqlalchemy.dialects.mysql import BIGINT

from trest.db import Model as Base


class UserScoreLog(Base):
    __tablename__ = 'user_score_log'

    id = Column(BIGINT(20), primary_key=True)
    user_id = Column(BIGINT(20), nullable=False, comment='用户ID')
    action = Column(String(40), nullable=False, server_default=text("''"))
    amount = Column(BIGINT(20), nullable=False, comment='操作后剩余积分总量')
    score = Column(BIGINT(20), comment='变动的积分数量，负数表示减少')
