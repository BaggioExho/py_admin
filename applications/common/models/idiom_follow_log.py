#!/usr/bin/env python
# -*- coding: utf-8 -*-
from sqlalchemy import Column, text
from sqlalchemy.dialects.mysql import BIGINT, TINYINT

from trest.db import Model as Base


class IdiomFollowLog(Base):
    __tablename__ = 'idiom_follow_log'

    id = Column(BIGINT(20), primary_key=True)
    idiom_follow_id = Column(BIGINT(20), comment='idiom_follow表自增长ID')
    user_id = Column(BIGINT(20), comment='用户ID')
    status = Column(TINYINT(1), nullable=False, server_default=text("'1'"), comment='状态:( 0 禁用；1 启用, 默认1 删除 -1)')
    created_at = Column(BIGINT(13), nullable=False, server_default=text("'0'"), comment='创建记录Unix时间戳毫秒单位')
