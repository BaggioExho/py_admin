#!/usr/bin/env python
# -*- coding: utf-8 -*-
from sqlalchemy import Column, JSON, String, text
from sqlalchemy.dialects.mysql import BIGINT, TINYINT

from trest.db import Model as Base


class UserCertification(Base):
    __tablename__ = 'user_certification'

    id = Column(BIGINT(20), primary_key=True)
    user_id = Column(BIGINT(20), nullable=False, server_default=text("'0'"), comment='主键，user表 id')
    realname = Column(String(40), nullable=False, server_default=text("''"), comment='真实姓名')
    type = Column(TINYINT(1), server_default=text("'0'"), comment='证件类型: 1 身份证认证  （目前只支持身份证）')
    number = Column(String(80), nullable=False, server_default=text("''"), comment='证件号码')
    attach = Column(JSON, nullable=False, comment='证件照片（json格式便于扩展）1 {"front":"","back":""}')
    authorized = Column(TINYINT(1), nullable=False, server_default=text("'0'"), comment='认证状态:( 0 待审核；1 审核通过, 2 审核失败)')
    client = Column(TINYINT(1), comment='客户端：1 iOS 2 Android 3 H5 4 PC  微信小程序')
    ip = Column(String(40), comment='添加记录的IP地址')
    expired_at = Column(BIGINT(13), comment='实名认证有效期，如果超过该事件，请要求重新认证')
    updated_at = Column(BIGINT(13), comment='更新记录UTC时间')
    created_at = Column(BIGINT(13), comment='创建记录UTC时间')
    status = Column(TINYINT(1), nullable=False, server_default=text("'1'"), comment='状态: -1 删除  0 禁用  1 启用')
    remark = Column(String(200), comment='备注；如果审核不通过，填写原因')
    authorized_admin_uid = Column(BIGINT(20), nullable=False, server_default=text("'0'"), comment='审核管理员ID，admin_user 的id； 0 表示为机审')
