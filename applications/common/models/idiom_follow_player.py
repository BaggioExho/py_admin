#!/usr/bin/env python
# -*- coding: utf-8 -*-
from sqlalchemy import Column, text
from sqlalchemy.dialects.mysql import BIGINT, TINYINT

from trest.db import Model as Base


class IdiomFollowPlayer(Base):
    __tablename__ = 'idiom_follow_player'

    id = Column(BIGINT(20), primary_key=True)
    idiom_follow_id = Column(BIGINT(20), nullable=False, comment='idiom_follow表自增长ID')
    user_id = Column(BIGINT(20), nullable=False, comment='用户ID')
    status = Column(TINYINT(1), nullable=False, server_default=text("'0'"), comment='状态: 0 未加入（未接受邀请）  1 加入  2 赢家  3 结束')
    begin_at = Column(BIGINT(13), nullable=False, server_default=text("'0'"), comment='Unix时间戳毫秒单位')
    end_at = Column(BIGINT(13), nullable=False, server_default=text("'0'"), comment='Unix时间戳毫秒单位')
    created_at = Column(BIGINT(13), nullable=False, server_default=text("'0'"), comment='创建记录Unix时间戳毫秒单位')
