#!/usr/bin/env python
# -*- coding: utf-8 -*-
from sqlalchemy import Column, String, text
from sqlalchemy.dialects.mysql import BIGINT, TINYINT

from trest.db import Model as Base


class IdiomInput(Base):
    __tablename__ = 'idiom_input'

    id = Column(BIGINT(20), primary_key=True)
    word = Column(String(80), nullable=False, server_default=text("''"), comment='词')
    count = Column(BIGINT(13), nullable=False, server_default=text("'1'"), comment='用户输入次数')
    remark = Column(String(2000), server_default=text("''"), comment='备注')
    status = Column(TINYINT(1), nullable=False, server_default=text("'0'"), comment='状态: -1 删除  0 禁用  1 启用')
    updated_at = Column(BIGINT(13), nullable=False, server_default=text("'0'"), comment='更新记录Unix时间戳毫秒单位')
    created_at = Column(BIGINT(13), nullable=False, server_default=text("'0'"), comment='创建记录Unix时间戳毫秒单位')
