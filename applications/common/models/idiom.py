#!/usr/bin/env python
# -*- coding: utf-8 -*-
from sqlalchemy import Column, String, text
from sqlalchemy.dialects.mysql import BIGINT, TINYINT

from trest.db import Model as Base


class Idiom(Base):
    __tablename__ = 'idiom'

    id = Column(BIGINT(20), primary_key=True)
    word = Column(String(80), nullable=False, server_default=text("''"), comment='词')
    pinyin = Column(String(120), nullable=False, server_default=text("''"), comment='拼音')
    derivation = Column(String(1600), nullable=False, server_default=text("''"), comment='词源')
    example = Column(String(1600), nullable=False, server_default=text("''"), comment='例子')
    explanation = Column(String(1600), nullable=False, server_default=text("''"), comment='解释')
    abbreviation = Column(String(40), nullable=False, server_default=text("''"), comment='缩写')
    status = Column(TINYINT(1), nullable=False, server_default=text("'1'"), comment='状态: -1 删除  0 禁用  1 启用')
    updated_at = Column(BIGINT(13), nullable=False, server_default=text("'0'"), comment='更新记录Unix时间戳毫秒单位')
    created_at = Column(BIGINT(13), nullable=False, server_default=text("'0'"), comment='创建记录Unix时间戳毫秒单位')
