#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""IdiomFollow 响应过滤器
"""


class IdiomFollowAssembler(object):
    @staticmethod
    def idiom_follow(idiom, follow, tips):
        data = {}
        data['idiom'] = IdiomFollowAssembler.filter_idiom(idiom)
        data['follow'] = IdiomFollowAssembler.filter_idiom(follow)
        data['tips'] = [IdiomFollowAssembler.filter_idiom(i) for i in tips]
        return data

    @staticmethod
    def filter_idiom(idiom):
        data = idiom.as_dict(['id', 'word', 'pinyin', 'derivation', 'example', 'explanation']) if idiom else {}
        if data:
            data['idiom_id'] = data['id']
            data.pop('id')
        return data
