#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""IdiomFollow控制器
"""
import tornado

from trest.router import get
from trest.router import put
from trest.router import post
from trest.router import delete
from trest.exception import JsonError
from trest.logger import syslogger

from applications.ishici.services.idiom import IdiomService
from applications.ishici.services.idiom_follow import IdiomFollowService
from applications.ishici.services.idiom_input import IdiomInputService


from applications.ishici.assemblers.idiom_follow import IdiomFollowAssembler

from .common import CommonHandler


class IdiomFollowHandler(CommonHandler):

    @post('/idiom_follow')
    # @tornado.web.authenticated
    def idiom_follow_post(self, *args, **kwargs):
        word = self.get_argument('idiom', None)
        idiom = IdiomService.get_by_word(word)
        if not idiom:
            IdiomInputService.count_input(word)
            raise JsonError('你确定输入的是成语？')

        follow = IdiomService.follow(word)
        if not follow:
            data = IdiomFollowAssembler.filter_idiom(idiom)
            raise JsonError(905,'你“艺高胆大”，别人没法接了', data=data)
        tips = IdiomService.follow_tips(follow.word) if follow else {}

        data = IdiomFollowAssembler.idiom_follow(idiom, follow, tips)
        return self.success(data = data)
