#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
from sqlalchemy.sql.expression import func
from trest.utils import utime
from trest.logger import SysLogger
from trest.config import settings
from trest.exception import JsonError
from applications.common.models.idiom import Idiom


class IdiomService(object):

    @staticmethod
    def follow_tips(idiom):
        """获取接龙的下一接龙的候选成语
        """
        c1 = idiom[-1]
        obj = Idiom.Q.filter(Idiom.word.like(f'{c1}%')).order_by(func.rand()).limit(10).all()
        return obj

    @staticmethod
    def follow(idiom):
        """获取输入成语的接龙
        """
        c1 = idiom[-1]
        obj = Idiom.Q.filter(Idiom.word.like(f'{c1}%')).order_by(func.rand()).first()
        return obj

    @staticmethod
    def get_by_word(word):
        """获取单条记录
        """
        obj = Idiom.Q.filter(Idiom.word == word).first()
        return obj

    @staticmethod
    def get(id):
        """获取单条记录

        [description]

        Arguments:
            id int -- 主键

        return:
            Idiom Model 实例 | None
        """
        if not id:
            raise JsonError('ID不能为空')
        obj = Idiom.Q.filter(Idiom.id == id).first()
        return obj
