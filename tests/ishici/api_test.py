#!/usr/bin/env python
# coding=utf-8
import unittest
from tornado.testing import AsyncHTTPTestCase
from server import application


class TestMain(AsyncHTTPTestCase):
    def get_app(self):
        return application
    def test_main(self):
        response = self.fetch('/')
        self.assertEqual(response.code, 200)
        self.assertEqual(response.body, b'Hello, world')
if __name__ == '__main__':
    unittest.main()
