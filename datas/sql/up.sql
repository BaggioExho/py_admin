

CREATE TABLE `idiom_input` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `word` varchar(80) NOT NULL DEFAULT '' COMMENT '词',
  `count` bigint(13) NOT NULL DEFAULT '1' COMMENT '用户输入次数',
  `remark` varchar(2000) DEFAULT '' COMMENT '备注',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态: -1 删除  0 禁用  1 启用',
  `updated_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '更新记录Unix时间戳毫秒单位',
  `created_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '创建记录Unix时间戳毫秒单位',
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_Word` (`word`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='为录入成语统计表';

CREATE TABLE `idiom_follow` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '接龙类型: 0 未知  1 人机对局  2 多人对局',
  `user_id` bigint(20) NOT NULL COMMENT '发起者用户ID',
  `begin_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT 'Unix时间戳毫秒单位',
  `end_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT 'Unix时间戳毫秒单位',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态: -1 删除  0 禁用  1 启用',
  `created_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '创建记录Unix时间戳毫秒单位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='成语接龙';

CREATE TABLE `idiom_follow_player` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idiom_follow_id` bigint(20) unsigned NOT NULL COMMENT 'idiom_follow表自增长ID',
  `user_id` bigint(20) unsigned NOT NULL COMMENT '用户ID',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态: 0 未加入（未接受邀请）  1 加入  2 赢家  3 结束',
  `begin_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT 'Unix时间戳毫秒单位',
  `end_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT 'Unix时间戳毫秒单位',
  `created_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '创建记录Unix时间戳毫秒单位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='成语接龙玩家表（理论上一局比赛赢家只有一个）';

CREATE TABLE `idiom_follow_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `idiom_follow_id` bigint(20) NOT NULL COMMENT 'idiom_follow表自增长ID',
  `user_id` bigint(20) NOT NULL COMMENT '用户ID',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态:( 0 禁用；1 启用, 默认1 删除 -1)',
  `created_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '创建记录Unix时间戳毫秒单位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='接龙记录';


CREATE TABLE `group` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) DEFAULT '0' COMMENT '类型: 1 公开圈子  2 私有圈子',
  `join_limit` tinyint(1) DEFAULT '0' COMMENT '加入限制: 1 不需审核  2 需要审核  3 只允许邀请加入',
  `content_limit` tinyint(1) DEFAULT '2' COMMENT '内部发布限制: 1 圈内不需审核  2 圈内需要审核  3 圈外需要审核',
  `owner_uid` bigint(20) NOT NULL COMMENT '圈子拥有者ID',
  `member_max` bigint(13) NOT NULL DEFAULT '50' COMMENT '允许最大成员数量',
  `member_count` bigint(13) NOT NULL DEFAULT '1' COMMENT '成员数量',
  `notification` varchar(800) NOT NULL DEFAULT '' COMMENT '公告',
  `introduction` varchar(2000) NOT NULL DEFAULT '' COMMENT '简介',
  `avatar` varchar(200) NOT NULL DEFAULT '' COMMENT '圈子头像',
  `name` varchar(80) NOT NULL DEFAULT '' COMMENT '圈子名称',

  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态: -1 删除  0 禁用  1 启用',
  `updated_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '更新记录Unix时间戳毫秒单位',
  `created_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '创建记录Unix时间戳毫秒单位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='圈子';

CREATE TABLE `group_apply_join` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,

  `group_id` bigint(20) NOT NULL COMMENT '圈子ID',
  `user_id` bigint(20) NOT NULL COMMENT '创建者用户ID',
  `alias` varchar(80) NOT NULL DEFAULT '' COMMENT '圈内别名',
  `description` varchar(800) DEFAULT '' COMMENT '成员描述',

  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态: 1 审核通过  2 待审核  3 拒绝加入',
  `option_uid` bigint(20) NOT NULL COMMENT '审核者用户ID',
  `remark` varchar(200) NOT NULL DEFAULT '' COMMENT '备注（拒绝理由等）',
  `updated_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '更新记录Unix时间戳毫秒单位',
  `created_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '创建记录Unix时间戳毫秒单位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='圈子加入申请（只能申请为普通成员）';

CREATE TABLE `group_invitation` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,

  `group_id` bigint(20) NOT NULL COMMENT '圈子ID',


  `inviter_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '邀请人',
  `invitee_id` bigint(20) NOT NULL DEFAULT '0' COMMENT '被邀请人',

  `role` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '被邀请成为角色: 1 成员  2 嘉宾 ',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态: 1 同意加入  2 待处理  3 拒绝加入',

  `link` varchar(200) NOT NULL DEFAULT '' COMMENT '邀请链接',
  `updated_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '更新记录Unix时间戳毫秒单位',
  `created_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '创建记录Unix时间戳毫秒单位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='圈子邀请加入（只能邀请为普通成员和嘉宾）';



CREATE TABLE `group_member` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) NOT NULL COMMENT '圈子ID',
  `user_id` bigint(20) NOT NULL COMMENT '创建者用户ID',
  `alias` varchar(80) NOT NULL DEFAULT '' COMMENT '圈内别名',
  `description` varchar(800) DEFAULT '' COMMENT '成员描述',


  `role` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '角色: 1 成员  2 嘉宾  3  管理员 4 圈主',

  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态: -2 主动退出  -1 被踢出  0 禁言  1 启用',
  `updated_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '更新记录Unix时间戳毫秒单位',
  `created_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '创建记录Unix时间戳毫秒单位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='圈子成员表（删除即表示成员被踢出圈子，updated_at为被踢出的时间，同时有踢出记录）';

CREATE TABLE `group_member_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `type` tinyint(1) DEFAULT '0' COMMENT '内容类型: 1 转让  2 被退出  3 主动退出',
  `group_member_id` bigint(20) NOT NULL COMMENT 'group_member表ID',
  `group_id` bigint(20) NOT NULL COMMENT '圈子ID',
  `user_id` bigint(20) NOT NULL COMMENT '圈子“转让、被踢出、主动退出”用户ID',
  `option_uid` bigint(20) NOT NULL DEFAULT 0 COMMENT '操作者用户ID（0 表示主动退出）',
  `remark` varchar(200) DEFAULT '' COMMENT '备注（踢出理由等）',
  `created_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '创建记录Unix时间戳毫秒单位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='圈子成员关系变更记录（圈子转让、主动退出、被踢出等等的时候记录到改表）';

CREATE TABLE `group_topic` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) NOT NULL COMMENT '圈子ID',
  `user_id` bigint(20) NOT NULL COMMENT '创建者用户ID',

  `type` tinyint(1) DEFAULT '0' COMMENT '内容类型: 1 文本  2 图文  3 图片  4 视频  5 文字视频  6 分享',
  `content` varbinary(4096) NOT NULL DEFAULT '' COMMENT '动态内容信息',
  `extend` json DEFAULT NULL COMMENT '扩展内容（json格式）：type = 2 图文（不超过N张） [url1,url2,]  type = 3 图片（不超过N张） [url1,url2,]，格式待定  type = 4、5 视频，格式待定  type = 6 格式为 [{"title":"", "thumbnail":"", "link":""}]',
  `longitude` decimal(12,9) NOT NULL DEFAULT '0.000000000' COMMENT '位置经度',
  `latitude` decimal(12,9) NOT NULL DEFAULT '0.000000000' COMMENT '位置纬度',
  `comment_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '评论数（评论+回复的总条数）',
  `favour_count` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '点赞数',
  `visibility` tinyint(1) DEFAULT '3' COMMENT '可见性： 1 公开  2 私密（仅自己可见）  3 圈内公开  4 部分可见（选择成员可见）  5 圈内不给谁看（选择成员不可见）',
  `visibility_uids` json DEFAULT NULL COMMENT 'visibility = 4 | 5 的时候有效',

  `status` tinyint(1) NOT NULL DEFAULT '2' COMMENT '状态: -1 删除  0 禁用  1 启用  2 审核中',
  `updated_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '更新记录Unix时间戳毫秒单位',
  `created_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '创建记录Unix时间戳毫秒单位',
  `msg_md5` char(32) NOT NULL DEFAULT '' COMMENT 'post的msg参数的md5值，用于防止重复提交',
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_MsgMd5` (`msg_md5`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='圈子话题';

CREATE TABLE `group_topic_audit` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) NOT NULL COMMENT '圈子ID',
  `topic_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '动态内容id',
  `audit_uid` bigint(20) NOT NULL COMMENT '审核者用户ID',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态: 1 通过  1 不通过',
  `remark` varchar(200) DEFAULT '' COMMENT '备注（不通过理由等）',
  `created_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '创建记录Unix时间戳毫秒单位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='圈子话题审计表';

CREATE TABLE `group_topic_reply` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) NOT NULL COMMENT '圈子ID',
  `user_id` bigint(20) NOT NULL COMMENT '创建者用户ID',
  `topic_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '动态内容id',
  `parent_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '父回复ID，为0表示为评论；大于0，表示对评论的回复',
  `comment_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '评论ID，0表示为评论',
  `reply_count` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '评论的回复数量',
  `content` varchar(800) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '' COMMENT '评论/回复内容信息',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '状态: -2 主动退出  -1 被踢出  0 禁用  1 启用',
  `created_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '创建记录Unix时间戳毫秒单位',
  `msg_md5` char(32) NOT NULL DEFAULT '' COMMENT 'post的msg参数的md5值，用于防止重复提交',
  PRIMARY KEY (`id`),
  UNIQUE KEY `u_MsgMd5` (`msg_md5`),
  KEY `i_Status_TopicID_UID_ParentID` (`status`,`topic_id`,`user_id`,`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='圈子话题回复表';

CREATE TABLE `group_topic_favour` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL COMMENT '创建者用户ID',
  `group_id` bigint(20) NOT NULL COMMENT '圈子ID',
  `topic_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '动态内容id',
  `status` tinyint(1) DEFAULT '0' COMMENT '状态：0 已取消赞  1 有效赞',
  `created_at` bigint(13) unsigned NOT NULL DEFAULT '0' COMMENT '创建记录Unix时间戳毫秒单位',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='圈子话题点赞表';

CREATE TABLE `group_timeline` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` bigint(20) NOT NULL COMMENT '圈子ID',
  `user_id` bigint(20) NOT NULL COMMENT '创建者用户ID',
  `topic_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '动态内容id',
  `is_own` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '是否是自己的',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='圈子轴表（审核通过的才有）';


